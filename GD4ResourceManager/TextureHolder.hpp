#pragma once
#include <map>
#include <memory>
#include <string>
#include <cassert>
#include "SFML/Graphics.hpp"

//namespace Textures
//{
//	enum ID{Landscape, Airplane};
//}

enum class TextureID {Landscape, Airplane};

class TextureHolder
{
private:
	std::map<TextureID, std::unique_ptr<sf::Texture>> mTextureMap;

private:
	void insertTexture(TextureID id, std::unique_ptr<sf::Texture> texture);
public:
	void load(TextureID id, const std::string& filename);
	sf::Texture& get(TextureID id);
	const sf::Texture& get(TextureID id) const;
};