#include "SFML/Graphics.hpp"
//#include "TextureHolder.hpp"
#include "ResourceHolder.hpp"
#include <iostream>

enum class TextureID { Landscape, Airplane };

int main()
{
	sf::RenderWindow window(sf::VideoMode(640, 480), "Resource Manager");

	//Try and load resources
	ResourceHolder<sf::Texture, TextureID> textures;
	try
	{
		textures.load(TextureID::Landscape, "Media/Textures/Desert.png");
		textures.load(TextureID::Airplane, "Media/Textures/Eagle.png");
	}
	catch (std::runtime_error& e)
	{
		std::cout << "Exception: " << e.what() << std::endl;
		return 1;
	}
	//Access the resources
	sf::Sprite landscape(textures.get(TextureID::Landscape));
	sf::Sprite airplane(textures.get(TextureID::Airplane));
	airplane.setPosition(200.f, 200.f);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::KeyReleased || event.type == sf::Event::Closed)
			{
				return 0;
			}
			window.clear();
			window.draw(landscape);
			window.draw(airplane);
			window.display();
		}
	}
	
}