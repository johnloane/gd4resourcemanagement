#include "TextureHolder.hpp"

void TextureHolder::load(TextureID id, const std::string& filename)
{
	//Create and load the resource
	std::unique_ptr<sf::Texture> texture(new sf::Texture());

	if (!texture->loadFromFile(filename))
	{
		throw std::runtime_error("TextureHolder::load - Failed to load " + filename);
	}
	//If the load was successful, insert texture into the map
	insertTexture(id, std::move(texture));
}

void TextureHolder::insertTexture(TextureID id, std::unique_ptr<sf::Texture> texture)
{
	//Insert and check the resource
	auto inserted = mTextureMap.insert(std::make_pair(id, std::move(texture)));
	assert(inserted.second);
}

sf::Texture& TextureHolder::get(TextureID id)
{
	auto found = mTextureMap.find(id);
	assert(found != mTextureMap.end());
	return *found->second;
}

const sf::Texture& TextureHolder::get(TextureID id) const
{
	auto found = mTextureMap.find(id);
	assert(found != mTextureMap.end());
	return *found->second;
}
